import './App.css';
import Buttons from "./Components/Buttons/Buttons";
import FirstList from "./Components/FirstList/FirstList";
import Pizza from '../src/assets/pizza_PNG43985.png';
import Coffee from '../src/assets/Coffee.jpeg';
import Lagman from '../src/assets/Lagman.jpeg';
import Sushi from '../src/assets/Sushi.jpeg';
import Plov from '../src/assets/Plov.jpeg';
import Tea from '../src/assets/Tea.jpeg';
import {useState} from "react";
import Orders from "./Components/Orders/Orders";
import {nanoid} from "nanoid";

const App = () => {
    const [order, setOrder] = useState([]);
    const [total, setTotal] = useState(0);

  const Menu = [
    {name: 'Coffee', price: 100, image: Coffee},
    {name: 'Pizza', price: 300, image: Pizza},
    {name: 'Lagman', price: 150, image: Lagman},
    {name: 'Sushi', price: 250, image: Sushi},
    {name: 'Plov', price: 80, image: Plov},
    {name: 'Tea', price: 20, image: Tea},
  ];

  if (order.length === 0) {
      setOrder([
          {name: 'Coffee', count: 0, id:nanoid()},
          {name: 'Pizza', count: 0, id:nanoid()},
          {name: 'Lagman', count: 0, id:nanoid()},
          {name: 'Sushi', count: 0, id:nanoid()},
          {name: 'Plov', count: 0, id:nanoid()},
          {name: 'Tea', count: 0, id:nanoid()},
      ]);
  }

  let number = 0;



  const addOrders = name => {
      setOrder(order.map((p, i) => {
          if (p.name === name) {
              setTotal(total + Menu[i].price);
              return {...p, count: p.count + 1};
          }
          return p;
      }))
  };


  const Delete = name => {
      setOrder(order.map((p, i) => {
          if (p.name === name) {
              setTotal(total - (p.count * Menu[i].price));
              return {...p, count: 0};
          }
          return p;
      }))
  }

  const buttons = Menu.map((p) => (
      <Buttons
          name={p.name}
          key={p.name}
          price={p.price}
          image={p.image}
          add={() => addOrders(p.name)}
      />
  ));

  const orders = order.map((p, i) => {
   if (p.count > 0) {
       number ++;
       return (
           <Orders
               key={p.id}
               name={p.name}
               counter={p.count}
               price={Menu[i].price}
               delete={() => Delete(p.name)}
           />
       );
   }
   return '';
});



  return (
    <div className="App">
      <div className='elements'>{buttons}</div>
      <FirstList
          title='Order is empty!'
          text='Please add some items'
          number={number}
          div={orders}
          total={total}
      />
    </div>
  );
}

export default App;
