import React from 'react';
import '../FirstList/FirstList.css';

const FirstList = props => {
    let classes = 'list';

    if (props.number !== 0) {
        classes = 'none';
    }
    
    if (props.number !== 0) {
        return (
            <div className='list'>
                <h3 style={{marginTop: '0'}}>Order Details:</h3>
                <ul>{props.div}</ul>
                <b>Total:{props.total}</b>
            </div>
        );
    }

    return (
        <div className={classes}>
            <p>{props.title}</p>
            <p>{props.text}</p>
        </div>
    );
};

export default FirstList;