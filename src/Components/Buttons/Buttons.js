import React from 'react';
import '../Buttons/Button.css';

const Buttons = props => {
    return (
        <div className='button' onClick={props.add}>
            <img style={{width:'70px'}} src={props.image} alt=""/>
            <h2>{props.name}</h2>
            <p>Price: {props.price}</p>
        </div>
    );
};

export default Buttons;