import React from 'react';
import '../Orders/Orders.css';

const Orders = props => {
    return (
      <li className='li'>
          <span className='ing'>{props.name}</span>
          <span className='count'>x{props.counter}</span>
          <span className='price'>{props.price}KGS</span>
          <button className='btn' type='button' onClick={props.delete}>X</button>
      </li>
    );
};

export default Orders;